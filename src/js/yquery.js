/*
 * yQuery 1.0.0
 *
 * Based on jQuery and the work to de-implement jQuery done by GitHub:
 * https://githubengineering.com/removing-jquery-from-github-frontend/
 */
const yQuery = function(selector) {
  return new yQuery.fn.init(selector); // eslint-disable-line new-cap
};

yQuery.fn = yQuery.prototype = { // eslint-disable-line no-multi-assign
  /**
   * @param selector
   * @returns {*}
   */
  init(selector) {
    if (!selector) {
      return yQuery;
    }

    if (typeof selector === "string") {
      const nodeList = document.querySelectorAll(selector);
      return Array.prototype.slice.call(nodeList);
    }

    if (yQuery.fn.isFunction(selector)) {
      if (document.attachEvent
        ? document.readyState === "complete"
        : document.readyState !== "loading"
      ) {
        selector();
      } else {
        document.addEventListener('DOMContentLoaded', selector);
      }
    }

    return yQuery;
  },

  /**
   * @param obj
   * @returns {boolean}
   */
  isFunction(obj) {
    return typeof obj === "function" && typeof obj.nodeType !== "number";
  },

  /**
   * @param array
   * @param callback
   * @returns {Array}
   */
  processNodeArray(array, callback) {
    let ret = [];
    array.forEach((item, index) => {
      ret = Array.prototype.concat.call(
        ret,
        callback(item)
      );
    });
    return ret;
  },

  /**
   * @param selector
   * @param origin
   * @returns {*}
   */
  getNodeArray(selector, origin) {
    if (selector instanceof Element) {
      return [selector];
    }

    if (!origin) {
      origin = document;
    }

    if (Array.isArray(origin)) {
      return yQuery.fn.processNodeArray(origin, (element) => {
        return yQuery.fn.getNodeArray(selector, element);
      });
    }

    const nodeList = origin.querySelectorAll(selector);
    return Array.prototype.slice.call(nodeList);
  },
};

yQuery.console = {
  debugMode: false,
  /**
   * @returns {boolean}
   */
  loggingDisabled() {
    return yQuery.console.debugMode || typeof console === 'undefined';
  },
  /**
   * @param arguments
   */
  warn() {
    if (yQuery.console.loggingDisabled()) {
      return;
    }
    console.warn.apply(console, arguments); // eslint-disable-line prefer-spread, prefer-rest-params
  },
  /**
   * @param arguments
   */
  log() {
    if (yQuery.console.loggingDisabled()) {
      return;
    }
    console.log.apply(console, arguments); // eslint-disable-line prefer-spread, prefer-rest-params
  },
  /**
   * @param arguments
   */
  error() {
    throw arguments.join(" ");
  },
};

/**
 * @param selector
 * @param origin
 * @returns {boolean}
 */
yQuery.exists = (selector, origin) => {
  const res = yQuery.fn.getNodeArray(selector, origin);
  return res.length > 0;
};

/**
 * @param selector
 * @param origin
 * @returns {*}
 */
yQuery.first = (selector, origin) => {
  const res = yQuery.fn.getNodeArray(selector, origin);
  if (res.length > 1) {
    yQuery.console.warn('yQuery: One node expected, found', res.length, 'for query "' + selector + '".');
  }
  return res[0];
};

/**
 * @param selector
 * @param origin
 * @returns {*}
 */
yQuery.many = (selector, origin) => {
  const res = yQuery.fn.getNodeArray(selector, origin);
  if (res.length < 2) {
    yQuery.console.warn('yQuery: Many nodes expected, found', res.length, 'for query "' + selector + '".');
  }
  return res;
};

/**
 * @param selector
 * @param origin
 * @returns {*}
 */
yQuery.closest = (selector, origin) => {
  if (Array.isArray(origin)) {
    return yQuery.fn.processNodeArray(origin, (element) => {
      return yQuery.closest(selector, element);
    });
  }
  return origin.closest(selector);
};

/**
 * @param url
 * @param settings
 * @returns {Promise<Response>}
 */
yQuery.fetch = (url, settings) => {
  return fetch(url, settings);
};

/**
 * @param eventName
 * @param eventTarget
 * @param eventHandler
 */
yQuery.on = (eventName, eventTarget, eventHandler) => {
  // Process all events separately
  if (Array.isArray(eventName)) {
    eventName.forEach((event) => {
      yQuery.on(event, eventTarget, eventHandler);
    });
    return;
  }

  // Process all targets separately
  if (Array.isArray(eventTarget)) {
    eventTarget.forEach((target) => {
      yQuery.on(eventName, target, eventHandler);
    });
    return;
  }

  // Add listener
  eventTarget.addEventListener(eventName, eventHandler);
};

export { yQuery };
