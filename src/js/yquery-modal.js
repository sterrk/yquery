/*
 * yQuery-modal 0.0.1
 */
import { yQuery } from "./yquery";
import { yAnimations } from "./yquery-animations";

const modalBg = (() => {
  const ret = document.createElement("div");
  ret.classList.add('yquery-modal-bg', 'close-yquery-modal');
  ret.style.display = 'none';
  ret.id = 'yquery-modal-bg';
  yQuery.one('body').appendChild(ret);
  return ret;
})();

/**
 * @param selector
 * @param action
 * @param origin
 */
const yModal = {
  open(selector, origin) {
    const selectedModal = yQuery.one(selector);
    selectedModal.style.top = '100px';
    selectedModal.style.visibility = 'visible';

    selectedModal.classList.add('open');
    yAnimations.fadeIn(modalBg);
    yAnimations.fadeIn(selectedModal);
    selectedModal.dispatchEvent(new CustomEvent(
      'yquery.modal.open',
      { detail: { caller: origin } }
    ));
  },
  close(selector, origin) {
    const selectedModal = yQuery.one(selector);
    selectedModal.style.top = '100px';
    selectedModal.style.visibility = 'visible';

    selectedModal.classList.remove('open');
    yAnimations.fadeOut(modalBg);
    yAnimations.fadeOut(selectedModal);
    selectedModal.dispatchEvent(new CustomEvent(
      'yquery.modal.close',
      { detail: { caller: origin } }
    ));
  },
};


// Attach modals based on .{open|close}-y-modal classes
yQuery.on('click', yQuery('.open-yquery-modal'), function(event) {
  event.preventDefault();
  const modalSelector = this.getAttribute('data-modal');
  if (!modalSelector) {
    console.error('yQuery-modal: Data-modal property not defined.');
    return;
  }
  modalBg.setAttribute('data-modal', modalSelector);
  yModal.open(modalSelector, this);
});

yQuery.on('click', yQuery('.close-yquery-modal'), function(event) {
  event.preventDefault();
  const modalSelector = this.getAttribute('data-modal');
  if (!modalSelector) {
    console.error('yQuery-modal: Data-modal property not defined.');
    return;
  }
  yModal.close(modalSelector, this);
});

export { yModal };
