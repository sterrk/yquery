const yAnimations = {
  fadeOut(element) {
    element.style.opacity = 1;

    (function fade() {
      element.style.opacity -= 0.1;
      if (element.style.opacity < 0) {
        element.style.display = "none";
      } else {
        requestAnimationFrame(fade);
      }
    }());
  },
  fadeIn(element, display) {
    element.style.opacity = 0;
    element.style.display = display || "block";

    (function fade() {
      const val = parseFloat(element.style.opacity) + 0.1;
      if (!(val > 1)) {
        element.style.opacity = val;
        requestAnimationFrame(fade);
      }
    }());
  },
};

export { yAnimations };
