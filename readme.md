# yQuery

yQuery was built as a replacement for the ever-prevalent jQuery when we decided that jQuery was too big and unwieldy to fill our needs.
We wanted more control over what exactly a query returns, and we wanted to leverage all the new functionality and speed of vanilla JavaScript.

## Installation

`npm install yquery`

## Usage

```javascript
import yQuery from 'yquery';

// yQuery.first returns a single element and warns you if it finds multiple
const title = yQuery.first('#title');

// Since yQuery.first returns a DOM element you can manipulate it with vanilla JS
title.classList.add('font-weight-bold');

// If you want to find a child of an element, yQuery accepts an origin as a second parameter in its query functions
const titleImage = yQuery.first('img', title);

// yQuery.many returns an array of DOM elements and warns you if it doesn't find multiple
const postTitles = yQuery.many('.post-title');

// Since yQuery.many returns a JS array you can manipulate it as usual
postTitles.forEach((element) => {
    // yQuery.closest returns the first match traversing back up the DOM tree from an origin element
    const post = yQuery.closest('.post', element);
});

// yQuery also provides a document.ready polyfill similar to jQuery
yQuery(() => {
    // For added error checking, yQuery.exists checks if a query returns any elements
    if (yQuery.exists('.footer')) {
        console.log(yQuery.first('.footer').innerHTML);
    }
});

// yQuery.on provides a convenient way to add event listeners to elements
yQuery.on('click', title, (event) => {
    console.log(event);
});
```

Additionally, yQuery includes a minimal implementation for a generic modal, as well as a set of animations.
These are available in `yquery-modal.js` and `yquery-animations.js` respectively.
